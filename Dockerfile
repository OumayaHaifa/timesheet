FROM openjdk:8-jdk-alpine
EXPOSE 8083
ADD target/timesheet-*.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
