package tn.esprit.spring;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.services.EmployeServiceImpl;
import tn.esprit.spring.services.TimesheetServiceImpl;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TimesheetApplicationTests {
    @Autowired
	TimesheetServiceImpl timesheetService;
    
    @Autowired
	EmployeServiceImpl employeService ;
    
	private static final Logger logger = Logger.getLogger(TimesheetServiceImpl.class);
	
	@Test
	public void testAddTimeSheet() throws ParseException {
		logger.info("Inside testAddTimeSheet method");
        //Employé
		Employe employe = new Employe();
		employe.setId(1);
		employe.setNom("Jardak");
		employe.setPrenom("Youssef");
		employe.setPassword("test");
		employe.setActif(true);
		employe.setEmail("yjardak@esprit");
		employeService.addOrUpdateEmploye(employe);

        //Mission
        Mission mission = new Mission();
        mission.setId(4);
        mission.setDescription("c'est ma mission");
        mission.setName("mamission");
        timesheetService.ajouterMission(mission);
        logger.info("Mission Name : " + mission.getName());
        assertNotNull(timesheetService.ajouterMission(mission));
		
        //TimeSheet
        String date="01/01/2020";  
        Date dateDebut=new SimpleDateFormat("dd/MM/yyyy").parse(date); 
        
        date="01/01/2021";
        Date dateFin=new SimpleDateFormat("dd/MM/yyyy").parse(date); 

        timesheetService.ajouterTimesheet(4, 1, dateDebut, dateFin);

   
		logger.info("Exiting testAddTimeSheet method");
	}
	
	@Test
	public void testGetAllEmployesByMission() {
		logger.info("Inside testGetAllEmployesByMission method");
		List<Employe> mylist= timesheetService.getAllEmployeByMission(4);
		assertNotNull(mylist);
		logger.info("Exiting testGetAllEmployesByMission method");
	}
}
