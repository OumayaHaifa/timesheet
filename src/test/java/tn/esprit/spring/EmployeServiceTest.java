package tn.esprit.spring;
import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Employe
;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.services.EmployeServiceImpl;

@SpringBootTest
@RunWith(SpringRunner.class)
 class EmployeServiceTest {
	@Autowired
	
	EmployeServiceImpl employeService ; 
	private static final Logger logger = Logger.getLogger(EmployeServiceTest.class);
	@Autowired
	EmployeRepository employeRepository; 
	
	@Test
	 void testAddEmploye() throws ParseException {
		logger.info("Begin AddEmploye method");
		
		Employe employe = new Employe();
		employe.setNom("Braham");
		employe.setPrenom("Arwa");
		employe.setPassword("aabb");
		employe.setActif(true);
		employe.setEmail("arwa@esprit");
        logger.info("Employe name : " + employe.getNom());
        assertNotNull(employeService.addOrUpdateEmploye(employe));
        for(Employe emp : employeService.getAllEmployes()) {
        	if(emp.getNom().equals(employe.getNom())) {
        		logger.warn(employe.getNom() + " : already exist ! ");
        	}
        	if(emp.getEmail().equals(employe.getEmail())) {
        		logger.warn(employe.getEmail() + " :  email already exist ! ");
        	}
        }
		logger.info("Exiting testAddEmploye method");
		
		
	}
}
