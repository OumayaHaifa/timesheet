package tn.esprit.spring;

import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.services.EntrepriseServiceImpl;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EntrepriseServiceTest {

	@Autowired
	EntrepriseServiceImpl entrepriseService;
	private static final Logger logger = Logger.getLogger(EntrepriseServiceTest.class);
	
	@Test
	public void testAddEntreprise() throws ParseException {
		logger.info("Inside testAddEntreprise method");
		
        Entreprise entreprise1 = new Entreprise();
        entreprise1.setName("Ouma");
        entreprise1.setRaisonSocial("UK");
        logger.info("Entreprise Name : " + entreprise1.getName());
        assertNotNull(entrepriseService.ajouterEntreprise(entreprise1));
		logger.info("Exiting testAddEntreprise method");
	}
	
	@Test
	public void testRetreiveEntreprise() {
		logger.info("Inside testRetreiveClient method");
		List<Entreprise> mylist= entrepriseService.retrieveAllEntreprises();
		assertNotNull(mylist);
		logger.info("Exiting testAddEntreprise method");
	}
}
